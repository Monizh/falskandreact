import flask

app = flask.Flask("__main__")


@app.route("/")
def my_index():
    return flask.render_template("index.html", token="Hello Flask+React")

app.run(debug=True)


"""

REFERENCE VIDEO YOUTUBE
https://www.youtube.com/watch?v=YW8VG_U-m48&ab_channel=MiguelGrinbergMiguelGrinberg

"""